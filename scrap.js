const request = require('request');
const cheerio = require('cheerio');
const path = require('path');
const express = require('express');
const hbs = require('express-handlebars');
const app = express();
const fs = require('fs');
const axios = require('axios');

// define a url base
const BASE_URL = ""

app.engine('hbs', hbs({
    extname: 'hbs',
    defaultlayout: 'layout',
    layoutDir: __dirname + '/views/layouts/',
    helpers: {
      'ifThird': function (index, options) {
         if(index%3 == 0){
                  return options.fn(this);
             } else {
                  return options.inverse(this);
               }

      },
      'notEmpety': function (string){
        if(string == '' || ' ' || null) {
          return false;
        }else{
          return string;
        }
      }
    }
}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs')
app.use(express.static('public'));


app.get(BASE_URL + '/', function (request, response) {
    let champlinks = JSON.parse(fs.readFileSync('newchamplinks.json', function (err) {
        if (err) throw err;
    }));

                let args = { BASE_URL: BASE_URL,
        title: 'MobaFire Wrapper!',
        champs: champlinks,
        layout: 'layout'
    }
    response.render('index', args);
});

app.get(BASE_URL + '/league-of-legends/champion/:champname', function (req, response) {
    let url = "https://www.mobafire.com/league-of-legends/champion/";
    url += req.param('champname');
    var guides = [];

    axios.get(url).then(function (res) {
            const $ = cheerio.load(res.data);
            const championlist = $(".browse-list");

            let champs = championlist.children('a');
            champs.each(function () {
                // let link = $(this).attr('href');
                let rating = $(this).children('.browse-list__item__rating');
                let values = {
                    'title': $(this).children('.browse-list__item__desc').children('h3').text(),
                    'link': $(this).attr('href'),
                    'votes': rating
                        .children('.browse-list__item__rating__total')
                        .text()
                        .replace('Votes: ', ''),
                    'views': rating
                        .children('.browse-list__item__rating__views')
                        .text()
                        .replace('Views: ', ''),
                    'elo': $(this)
                        .attr('class')
                        .replace('browse-list__item', '')
                        .replace(' player-', ''),
                    'role': $(this)
                        .children('.browse-list__item__pic')
                        .attr('class')
                        .replace('browse-list__item__pic', '')
                        .replace(' role-', ''),
                    'tolltip': $(this).children('.browse-list__item__desc')
                        .children('ul')
                        .children('li.tolltip'),
                }
                // console.log($(this).attr('class'));
                guides.push(values);
            });

            let champnamee = req.param('champname').replace('/league-of-legends/champion/', '')
                    .replace(/-/g, ' ')
                    .replace(/\d+/g, '');
                        let args = { BASE_URL: BASE_URL,
                url: url,
                champname: champnamee,
                guides: guides,
                layout: 'layout'
                }

            response.render('guidelist', args);

        })
        .catch(function (error) {
            // handle error
            console.log(error);

            response.render('error', {
                error: error
            });
        })
});

app.get(BASE_URL + '/league-of-legends/build/:buildlink', function (req, response) {
    let url = "https://www.mobafire.com/league-of-legends/build/";
    url += req.param('buildlink');
    var guides = [];
    axios.get(url).then(function (res) {
            let re = new RegExp("src=\"", 'g');

            const $ = cheerio.load(res.data);
            // spells
            let spells = $(".build-spells").html();
            spells = spells.replace(re, "src=\"https://www.mobafire.com");
            // reforged runes
            let runesp = $(".new-runes").html();


            runesp = runesp.replace(re, "src=\"https://www.mobafire.com");

            // items
            let items = $(".item-wrap");
            let itemsHtml = '';
            items.each(function () {
                itemsHtml += $(this).html().replace(re, "src=\"https://www.mobafire.com");
            });
            let ability = $(".ability-wrap").html();
            ability = ability.replace(re, "src=\"https://www.mobafire.com");

            // items = items.replace(re, "src=\"https://www.mobafire.com");
                        let args = { BASE_URL: BASE_URL,
                url: url,
                reforgedRunes: runesp,
                items: itemsHtml,
                ability: ability,
                spells: spells,
                gtitle: $(".guide-main-title").text(),
                title: $("title"),
                layout: 'layout'
            }

            response.render('guide', args);

        })
        .catch(function (error) {
            // handle error
            console.log(error);

            response.render('error', {
                error: error
            });
        })
});


app.get(BASE_URL + '/atualiza-links', function (request, response) {
    let links = [];
    axios.get('https://www.mobafire.com/league-of-legends/champions').then(function(res) {
            const $ = cheerio.load(res.data);

            const championlist = $(".champ-list.champ-list--details.self-clear");

            let champs = championlist.children('a');
            champs.each( function () {
                let link = $(this).attr('href');
                links.push(link);
            });

            // console.log(links);
            fs.writeFileSync('champlinks.json', JSON.stringify(links), 'utf8')
    });

    let champlinks = JSON.parse(fs.readFileSync('newchamplinks.json', function (err) {
        if (err) throw err;
    }));


                let args = { BASE_URL: BASE_URL,
        title: 'MobaFire Wrapper!',
        champs: champlinks,
        total: champlinks.length,
        layout: 'layout'
    }

    response.render('ok', args);
});

app.listen(3000);
